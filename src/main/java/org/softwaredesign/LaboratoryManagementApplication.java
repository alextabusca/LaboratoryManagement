package org.softwaredesign;

import org.softwaredesign.backend.service.TokenGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class LaboratoryManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(LaboratoryManagementApplication.class, args);

        //System.out.println(TokenGenerator.randomAlphaNumeric());
    }
}