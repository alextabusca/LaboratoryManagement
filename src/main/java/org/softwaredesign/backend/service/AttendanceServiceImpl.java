package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.AttendanceDTO;
import org.softwaredesign.backend.dataAccess.repository.AttendanceRepository;
import org.softwaredesign.backend.model.AttendanceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    private final AttendanceRepository attendanceRepository;

    @Autowired
    public AttendanceServiceImpl(AttendanceRepository attendanceRepository) {
        this.attendanceRepository = attendanceRepository;
    }

    public List<AttendanceModel> getAllAttendances() {
        return attendanceRepository.findAll();
    }

    public AttendanceModel getAttendanceByID(Long attendanceID) {
        Optional<AttendanceModel> result = attendanceRepository.findById(attendanceID);

        return result.orElse(null);
    }

    public AttendanceModel saveAttendance(AttendanceDTO attendanceDTO) {
        AttendanceModel attendanceToBeSaved = new AttendanceModel(attendanceDTO.isPresent());

        return attendanceRepository.save(attendanceToBeSaved);
    }

    public AttendanceModel updateAttendance(Long attendanceID, AttendanceDTO attendanceDTO) {
        AttendanceModel attendanceToBeUpdated = attendanceRepository.getOne(attendanceID);

       /* if (attendanceDTO.isPresent() != null)
            attendanceToBeUpdated.setPresent(attendanceDTO.isPresent());*/

        return attendanceRepository.save(attendanceToBeUpdated);
    }

    public void deleteAttendanceByID(Long attendanceID) {
        attendanceRepository.deleteById(attendanceID);
    }

    public List<AttendanceModel> getAttendanceByLaboratoryID(Long laboratoryID) {
        return attendanceRepository.findByLaboratoryModel_laboratoryID(laboratoryID);
    }


}
