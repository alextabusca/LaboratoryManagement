package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.AssignmentDTO;
import org.softwaredesign.backend.model.AssignmentModel;

import java.util.List;

public interface AssignmentService {

    List<AssignmentModel> getAllAssignments();

    AssignmentModel getAssignmentByID(Long assignmentID);

    AssignmentModel saveAssignment(AssignmentDTO assignmentDTO);

    AssignmentModel updateAssignment(Long assignmentID, AssignmentDTO assignmentDTO);

    void deleteAssignmentByID(Long assignmentID);

    List<AssignmentModel> getAssignmentsByLaboratoryID(Long laboratoryID);

}
