package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.SubmissionDTO;
import org.softwaredesign.backend.dataAccess.repository.SubmissionRepository;
import org.softwaredesign.backend.model.SubmissionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    private final SubmissionRepository submissionRepository;

    @Autowired
    public SubmissionServiceImpl(SubmissionRepository submissionRepository) {
        this.submissionRepository = submissionRepository;
    }

    public List<SubmissionModel> getAllSubmissions() {
        return submissionRepository.findAll();
    }

    public SubmissionModel getSubmissionByID(Long submissionID) {
        Optional<SubmissionModel> result = submissionRepository.findById(submissionID);

        return result.orElse(null);
    }

    public SubmissionModel saveSubmission(SubmissionDTO submissionDTO) {
        SubmissionModel submissionToBeSaved = new SubmissionModel(submissionDTO.getDate(), submissionDTO.getDescription(), submissionDTO.getGrade());

        return submissionRepository.save(submissionToBeSaved);
    }

    public SubmissionModel updateSubmission(Long submissionID, SubmissionDTO submissionDTO) {
        SubmissionModel submissionToBeUpdated = submissionRepository.getOne(submissionID);

        if (submissionDTO.getDate() != null)
            submissionToBeUpdated.setDate(submissionDTO.getDate());

        if (submissionDTO.getDescription() != null)
            submissionToBeUpdated.setDescription(submissionDTO.getDescription());

        if (submissionDTO.getGrade() != submissionToBeUpdated.getGrade())
            submissionToBeUpdated.setGrade(submissionDTO.getGrade());

        return submissionRepository.save(submissionToBeUpdated);
    }

    public void deleteSubmissionByID(Long submissionID) {
        submissionRepository.deleteById(submissionID);
    }
}
