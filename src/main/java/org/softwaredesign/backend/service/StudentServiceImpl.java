package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.StudentDTO;
import org.softwaredesign.backend.dataAccess.repository.StudentRepository;
import org.softwaredesign.backend.model.StudentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<StudentModel> getAllStudents() {
        return studentRepository.findAll();
    }

    public StudentModel getStudentByID(Long studentID) {
        //return userRepository.getOne(userID);
        Optional<StudentModel> result = studentRepository.findById(studentID);

        /*if(result.isPresent())
            return result.get();
        else
            return null;*/

        return result.orElse(null);
    }

    public StudentModel login(String email, String password) {
        return studentRepository.findByEmailAndPassword(email, password);
    }

    public StudentModel register(String token, String email, String password) {
        StudentModel studentToBeRegistered = studentRepository.findByEmail(email);

        if (studentToBeRegistered.getToken().equals(token)) {
            studentToBeRegistered.setPassword(password);
            return studentRepository.save(studentToBeRegistered);
        } else
            return null;
    }

    public StudentModel saveStudent(StudentDTO studentDTO) {
        StudentModel studentToBeSaved = new StudentModel(studentDTO.getFirstName(), studentDTO.getLastName(), studentDTO.getInGroup(), studentDTO.getEmail(), studentDTO.getPassword(), studentDTO.getHobby());

        /*if (studentRepository.findByEmail(studentDTO.getEmail()) == null) {
            studentToBeSaved.setToken(TokenGenerator.randomTokenGenerator());
            return studentRepository.save(studentToBeSaved);
        } else
            return null;*/

        studentToBeSaved.setToken(TokenGenerator.randomTokenGenerator());
        return studentRepository.save(studentToBeSaved);

    }

    public StudentModel updateStudent(Long studentID, StudentDTO studentDTO) {
        StudentModel studentToBeUpdated = studentRepository.getOne(studentID);

        if (studentDTO.getFirstName() != null)
            studentToBeUpdated.setFirstName(studentDTO.getFirstName());

        if (studentDTO.getLastName() != null)
            studentToBeUpdated.setLastName(studentDTO.getLastName());

        if (studentDTO.getInGroup() != null)
            studentToBeUpdated.setInGroup(studentDTO.getInGroup());

        if (studentDTO.getEmail() != null)
            studentToBeUpdated.setEmail(studentDTO.getEmail());

        if (studentDTO.getHobby() != null)
            studentToBeUpdated.setHobby(studentDTO.getHobby());

        System.out.println(studentToBeUpdated.toString());

        return studentRepository.save(studentToBeUpdated);
    }

    public void deleteStudentByID(Long userID) {
        studentRepository.deleteById(userID);
    }

}
