package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.StudentDTO;
import org.softwaredesign.backend.model.StudentModel;

import java.util.List;

public interface StudentService {

    List<StudentModel> getAllStudents();

    StudentModel getStudentByID(Long studentID);

    StudentModel saveStudent(StudentDTO studentDTO);

    StudentModel updateStudent(Long studentID, StudentDTO studentDTO);

    StudentModel login(String email, String password);

    StudentModel register(String token, String email, String password);

    void deleteStudentByID(Long studentID);
}
