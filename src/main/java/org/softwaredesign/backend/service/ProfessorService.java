package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.ProfessorDTO;
import org.softwaredesign.backend.model.ProfessorModel;

import java.util.List;

public interface ProfessorService {

    List<ProfessorModel> professorLogin(String email, String password);

    List<ProfessorModel> getAllProfessors();

    ProfessorModel getProfessorByID(Long professorID);

    ProfessorModel saveProfessor(ProfessorDTO professorDTO);

    ProfessorModel updateProfessor(Long professorID, ProfessorDTO professorDTO);

    void deleteProfessorByID(Long professorID);
}
