package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.AssignmentDTO;
import org.softwaredesign.backend.dataAccess.dbModel.LaboratoryDTO;
import org.softwaredesign.backend.dataAccess.repository.AssignmentRepository;
import org.softwaredesign.backend.model.AssignmentModel;
import org.softwaredesign.backend.model.LaboratoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    public List<AssignmentModel> getAllAssignments() {
        return assignmentRepository.findAll();
    }

    public AssignmentModel getAssignmentByID(Long assignmentID) {
        Optional<AssignmentModel> result = assignmentRepository.findById(assignmentID);

        return result.orElse(null);
    }

    public List<AssignmentModel> getAssignmentsByLaboratoryID(Long laboratoryID) {
        return assignmentRepository.getByLaboratoryModel_laboratoryID(laboratoryID);
    }

    public AssignmentModel saveAssignment(AssignmentDTO assignmentDTO) {
        //AssignmentModel assignmentToBeSaved = new AssignmentModel(assignmentDTO.getTitle(), assignmentDTO.getDeadline(), assignmentDTO.getDescription(), laboratoryDTOToModel(assignmentDTO.getLaboratoryDTO()));

        AssignmentModel assignmentToBeSaved = new AssignmentModel(assignmentDTO.getTitle(), assignmentDTO.getDeadline(), assignmentDTO.getDescription());


        return assignmentRepository.save(assignmentToBeSaved);
    }

    public AssignmentModel updateAssignment(Long assignmentID, AssignmentDTO assignmentDTO) {
        AssignmentModel assignmentToBeUpdated = assignmentRepository.getOne(assignmentID);

        if (assignmentDTO.getTitle() != null)
            assignmentToBeUpdated.setTitle(assignmentDTO.getTitle());

        if (assignmentDTO.getDeadline() != null)
            assignmentToBeUpdated.setDeadline(assignmentDTO.getDeadline());

        if (assignmentDTO.getDescription() != null)
            assignmentToBeUpdated.setDescription(assignmentDTO.getDescription());

        return assignmentRepository.save(assignmentToBeUpdated);
    }

    public void deleteAssignmentByID(Long assignmentID) {
        assignmentRepository.deleteById(assignmentID);
    }

    private LaboratoryModel laboratoryDTOToModel(LaboratoryDTO laboratoryDTO) {
        LaboratoryModel laboratoryModel = new LaboratoryModel();

        laboratoryModel.setTitle(laboratoryDTO.getTitle());
        laboratoryModel.setDate(laboratoryDTO.getDate());
        laboratoryModel.setMainTopics(laboratoryDTO.getMainTopics());
        laboratoryModel.setLongDescription(laboratoryDTO.getLongDescription());

        return laboratoryModel;
    }
}
