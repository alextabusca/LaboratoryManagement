package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.AttendanceDTO;
import org.softwaredesign.backend.model.AttendanceModel;

import java.util.List;

public interface AttendanceService {

    List<AttendanceModel> getAllAttendances();

    AttendanceModel getAttendanceByID(Long attendanceID);

    AttendanceModel saveAttendance(AttendanceDTO attendanceDTO);

    AttendanceModel updateAttendance(Long attendanceID, AttendanceDTO attendanceDTO);

    void deleteAttendanceByID(Long attendanceID);

    List<AttendanceModel> getAttendanceByLaboratoryID(Long laboratoryID);
}
