package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.LaboratoryDTO;
import org.softwaredesign.backend.dataAccess.repository.LaboratoryRepository;
import org.softwaredesign.backend.model.LaboratoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LaboratoryServiceImpl implements LaboratoryService {

    private final LaboratoryRepository laboratoryRepository;

    @Autowired
    public LaboratoryServiceImpl(LaboratoryRepository laboratoryRepository) {
        this.laboratoryRepository = laboratoryRepository;
    }

    public List<LaboratoryModel> getAllLaboratories() {
        return laboratoryRepository.findAll();
    }

    public LaboratoryModel getLaboratoryByID(Long laboratoryID) {
        Optional<LaboratoryModel> result = laboratoryRepository.findById(laboratoryID);

        return result.orElse(null);
    }

    public LaboratoryModel saveLaboratory(LaboratoryDTO laboratoryDTO) {
        LaboratoryModel laboratoryToBeSaved = new LaboratoryModel(laboratoryDTO.getTitle(), laboratoryDTO.getDate(), laboratoryDTO.getMainTopics(), laboratoryDTO.getLongDescription());

        return laboratoryRepository.save(laboratoryToBeSaved);
    }

    public LaboratoryModel updateLaboratory(Long laboratoryID, LaboratoryDTO laboratoryDTO) {
        LaboratoryModel laboratoryToBeUpdated = laboratoryRepository.getOne(laboratoryID);

        if (laboratoryDTO.getTitle() != null)
            laboratoryToBeUpdated.setTitle(laboratoryDTO.getTitle());

        if (laboratoryDTO.getDate() != null)
            laboratoryToBeUpdated.setDate(laboratoryDTO.getDate());

        if (laboratoryDTO.getMainTopics() != null)
            laboratoryToBeUpdated.setMainTopics(laboratoryDTO.getMainTopics());

        if (laboratoryDTO.getLongDescription() != null)
            laboratoryToBeUpdated.setLongDescription(laboratoryDTO.getLongDescription());

        return laboratoryRepository.save(laboratoryToBeUpdated);
    }

    public void deleteLaboratoryByID(Long laboratoryID) {
        laboratoryRepository.deleteById(laboratoryID);
    }

    public List<LaboratoryModel> searchAfterKeyword(String keyword) {
        return laboratoryRepository.getAllByLongDescriptionContaining(keyword);
    }
}
