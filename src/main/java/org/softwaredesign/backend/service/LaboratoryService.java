package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.LaboratoryDTO;
import org.softwaredesign.backend.model.LaboratoryModel;

import java.util.List;

public interface LaboratoryService {

    List<LaboratoryModel> getAllLaboratories();

    LaboratoryModel getLaboratoryByID(Long laboratoryID);

    LaboratoryModel saveLaboratory(LaboratoryDTO laboratoryDTO);

    LaboratoryModel updateLaboratory(Long laboratoryID, LaboratoryDTO laboratoryDTO);

    List<LaboratoryModel> searchAfterKeyword(String keyword);

    void deleteLaboratoryByID(Long laboratoryID);
}
