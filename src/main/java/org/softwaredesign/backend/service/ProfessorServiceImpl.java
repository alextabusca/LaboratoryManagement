package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.ProfessorDTO;
import org.softwaredesign.backend.dataAccess.repository.ProfessorRepository;
import org.softwaredesign.backend.model.ProfessorModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessorServiceImpl implements ProfessorService {

    private final ProfessorRepository professorRepository;

    @Autowired
    public ProfessorServiceImpl(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    public List<ProfessorModel> professorLogin(String email, String password) {
        return professorRepository.findByEmailAndPassword(email, password);
    }

    public List<ProfessorModel> getAllProfessors() {
        return professorRepository.findAll();
    }

    public ProfessorModel getProfessorByID(Long professorID) {
        Optional<ProfessorModel> result = professorRepository.findById(professorID);

        return result.orElse(null);
    }

    public ProfessorModel saveProfessor(ProfessorDTO professorDTO) {
        ProfessorModel professorToBeSaved = new ProfessorModel(professorDTO.getFirstName(), professorDTO.getLastName(), professorDTO.getEmail(), professorDTO.getPassword());

        return professorRepository.save(professorToBeSaved);
    }

    public ProfessorModel updateProfessor(Long professorID, ProfessorDTO professorDTO) {
        ProfessorModel professorToBeUpdated = professorRepository.getOne(professorID);

        if (professorDTO.getFirstName() != null)
            professorToBeUpdated.setFirstName(professorDTO.getFirstName());

        if (professorDTO.getLastName() != null)
            professorToBeUpdated.setLastName(professorDTO.getLastName());

        if (professorDTO.getEmail() != null)
            professorToBeUpdated.setEmail(professorDTO.getEmail());

        if (professorDTO.getPassword() != null)
            professorToBeUpdated.setPassword(professorDTO.getPassword());

        return professorRepository.save(professorToBeUpdated);
    }

    public void deleteProfessorByID(Long professorID) {
        professorRepository.deleteById(professorID);
    }
}
