package org.softwaredesign.backend.service;

import org.softwaredesign.backend.dataAccess.dbModel.SubmissionDTO;
import org.softwaredesign.backend.model.SubmissionModel;

import java.util.List;

public interface SubmissionService {

    List<SubmissionModel> getAllSubmissions();

    SubmissionModel getSubmissionByID(Long submissionID);

    SubmissionModel saveSubmission(SubmissionDTO submissionDTO);

    SubmissionModel updateSubmission(Long submissionID, SubmissionDTO submissionDTO);

    void deleteSubmissionByID(Long submissionID);

}
