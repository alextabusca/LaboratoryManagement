package org.softwaredesign.backend.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Proxy(lazy = false)
@Table(name = "attendance")
public class AttendanceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "attendance_id")
    private long attendanceID;

    @ManyToOne
    @JoinColumn(name = "laboratory_id")
    private LaboratoryModel laboratoryModel;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private StudentModel studentModel;

    @Column(name = "is_present")
    private boolean isPresent;

    public AttendanceModel() {

    }

    public AttendanceModel(boolean isPresent) {
        this.isPresent = isPresent;
    }

    public long getAttendanceID() {
        return attendanceID;
    }

    public void setAttendanceID(long attendanceID) {
        this.attendanceID = attendanceID;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    public LaboratoryModel getLaboratoryModel() {
        return laboratoryModel;
    }

    public void setLaboratoryModel(LaboratoryModel laboratoryModel) {
        this.laboratoryModel = laboratoryModel;
    }

    public StudentModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(StudentModel studentModel) {
        this.studentModel = studentModel;
    }
}
