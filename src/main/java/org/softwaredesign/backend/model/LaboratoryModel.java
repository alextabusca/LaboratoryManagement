package org.softwaredesign.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "laboratory")
public class LaboratoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "laboratory_id")
    private Long laboratoryID;

    @Column(name = "title")
    private String title;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "date")
    private Date date;

    @Column(name = "main_topics")
    private String mainTopics;

    @Column(name = "long_description")
    private String longDescription;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "laboratoryModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AssignmentModel> assignmentModels;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "laboratoryModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AttendanceModel> attendanceModels;

    public LaboratoryModel() {

    }

    public LaboratoryModel(String title, Date date, String mainTopics, String longDescription) {
        this.title = title;
        this.date = date;
        this.mainTopics = mainTopics;
        this.longDescription = longDescription;
    }

    public Long getLaboratoryID() {
        return laboratoryID;
    }

    public void setLaboratoryID(Long laboratoryID) {
        this.laboratoryID = laboratoryID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMainTopics() {
        return mainTopics;
    }

    public void setMainTopics(String mainTopics) {
        this.mainTopics = mainTopics;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Set<AssignmentModel> getAssignmentModels() {
        return assignmentModels;
    }

    public void setAssignmentModels(Set<AssignmentModel> assignmentModels) {
        this.assignmentModels = assignmentModels;
    }

    public Set<AttendanceModel> getAttendanceModels() {
        return attendanceModels;
    }

    public void setAttendanceModels(Set<AttendanceModel> attendanceModels) {
        this.attendanceModels = attendanceModels;
    }
}
