package org.softwaredesign.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "assignment")
public class AssignmentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "assignment_id")
    private long assignmentID;

    @Column(name = "title")
    private String title;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "deadline")
    private Date deadline;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "laboratory_id")
    private LaboratoryModel laboratoryModel;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "assignmentModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SubmissionModel> submissionModels;

    public AssignmentModel() {
    }

    public AssignmentModel(String title, Date deadline, String description) {
        this.title = title;
        this.deadline = deadline;
        this.description = description;
        this.laboratoryModel = laboratoryModel;
    }

    public long getAssignmentID() {
        return assignmentID;
    }

    public void setAssignmentID(long assignmentID) {
        this.assignmentID = assignmentID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LaboratoryModel getLaboratoryModel() {
        return laboratoryModel;
    }

    public void setLaboratoryModel(LaboratoryModel laboratoryModel) {
        this.laboratoryModel = laboratoryModel;
    }

    public Set<SubmissionModel> getSubmissionModels() {
        return submissionModels;
    }

    public void setSubmissionModels(Set<SubmissionModel> submissionModels) {
        this.submissionModels = submissionModels;
    }
}
