package org.softwaredesign.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Proxy(lazy = false)
@Table(name = "submission")
public class SubmissionModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "submission_id")
    private long submissionID;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "date")
    private Date date;

    @Column(name = "description")
    private String description;

    @Column(name = "grade")
    private int grade;


    @ManyToOne
    @JoinColumn(name = "assignment_id")
    private AssignmentModel assignmentModel;


    @ManyToOne
    @JoinColumn(name = "student_id")
    private StudentModel studentModel;

    public SubmissionModel() {

    }

    public SubmissionModel(Date date, String description, int grade) {
        this.date = date;
        this.description = description;
        this.grade = grade;
    }

    public long getSubmissionID() {
        return submissionID;
    }

    public void setSubmissionID(long submissionID) {
        this.submissionID = submissionID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public AssignmentModel getAssignmentModel() {
        return assignmentModel;
    }

    public void setAssignmentModel(AssignmentModel assignmentModel) {
        this.assignmentModel = assignmentModel;
    }

    public StudentModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(StudentModel studentModel) {
        this.studentModel = studentModel;
    }
}
