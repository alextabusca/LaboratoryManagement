package org.softwaredesign.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Proxy(lazy = false)
@Table(name = "student")
public class StudentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_id")
    private long studentID;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "in_group")
    private String inGroup;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "hobby")
    private String hobby;

    @Column(name = "token")
    private String token;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "studentModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AttendanceModel> attendanceModels;

    @JsonIgnore
    @JsonBackReference
    @OneToMany(mappedBy = "studentModel", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SubmissionModel> submissionModels;

    public StudentModel() {

    }

    public StudentModel(String firstName, String lastName, String inGroup, String email, String password, String hobby) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.inGroup = inGroup;
        this.email = email;
        this.password = password;
        this.hobby = hobby;
    }

    public long getStudentID() {
        return studentID;
    }

    public void setStudentID(long studentID) {
        this.studentID = studentID;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInGroup() {
        return inGroup;
    }

    public void setInGroup(String inGroup) {
        this.inGroup = inGroup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String passoword) {
        this.password = passoword;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<AttendanceModel> getAttendanceModels() {
        return attendanceModels;
    }

    public void setAttendanceModels(Set<AttendanceModel> attendanceModels) {
        this.attendanceModels = attendanceModels;
    }

    public Set<SubmissionModel> getSubmissionModels() {
        return submissionModels;
    }

    public void setSubmissionModels(Set<SubmissionModel> submissionModels) {
        this.submissionModels = submissionModels;
    }

    public String toString() {
        return "User{" + "studentID=" + studentID + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' + '}';
    }

}
