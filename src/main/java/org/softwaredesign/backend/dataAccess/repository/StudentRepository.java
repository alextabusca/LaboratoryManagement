package org.softwaredesign.backend.dataAccess.repository;

import org.softwaredesign.backend.model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface StudentRepository extends JpaRepository<StudentModel, Long> {

    StudentModel findByEmail(String email);
    StudentModel findByEmailAndPassword(String email, String password);
}
