package org.softwaredesign.backend.dataAccess.repository;

import org.softwaredesign.backend.model.SubmissionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SubmissionRepository extends JpaRepository<SubmissionModel, Long> {
    
}
