package org.softwaredesign.backend.dataAccess.repository;

import org.softwaredesign.LaboratoryManagementApplication;
import org.softwaredesign.backend.model.LaboratoryModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface LaboratoryRepository extends JpaRepository<LaboratoryModel, Long> {

    List<LaboratoryModel> getAllByLongDescriptionContaining(String keyword);
}
