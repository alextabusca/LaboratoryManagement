package org.softwaredesign.backend.dataAccess.repository;

import org.softwaredesign.backend.model.AttendanceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AttendanceRepository extends JpaRepository<AttendanceModel, Long> {

    List<AttendanceModel> findByLaboratoryModel_laboratoryID(Long laboratoryID);
}
