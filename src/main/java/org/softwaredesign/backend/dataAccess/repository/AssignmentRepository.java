package org.softwaredesign.backend.dataAccess.repository;

import org.softwaredesign.backend.model.AssignmentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AssignmentRepository extends JpaRepository<AssignmentModel, Long> {

    List<AssignmentModel> getByLaboratoryModel_laboratoryID(Long laboratoryID);

}
