package org.softwaredesign.backend.dataAccess.dbModel;

import java.io.Serializable;

public class StudentDTO implements Serializable {

    private String firstName;
    private String lastName;
    private String inGroup;
    private String email;
    private String password;
    private String hobby;
    private String token;

    public StudentDTO() {

    }

    public StudentDTO(String firstName, String lastName, String inGroup, String email, String passwor, String hobby) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.inGroup = inGroup;
        this.email = email;
        this.password = passwor;
        this.hobby = hobby;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInGroup() {
        return inGroup;
    }

    public void setInGroup(String inGroup) {
        this.inGroup = inGroup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String toString() {
        return "StudentDTO{" + "firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' + '}';
    }
}
