package org.softwaredesign.backend.dataAccess.dbModel;

public class AttendanceDTO {

    private boolean isPresent;

    public AttendanceDTO() {

    }

    public AttendanceDTO(boolean isPresent) {
        this.isPresent = isPresent;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }
}
