package org.softwaredesign.backend.dataAccess.dbModel;

import java.io.Serializable;
import java.sql.Date;

public class SubmissionDTO implements Serializable {

    private Date date;
    private String description;
    private int grade;

    public SubmissionDTO() {

    }

    public SubmissionDTO(Date date, String description, int grade) {
        this.date = date;
        this.description = description;
        this.grade = grade;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
