package org.softwaredesign.backend.dataAccess.dbModel;

import java.io.Serializable;
import java.util.Date;

public class LaboratoryDTO implements Serializable {

    private String title;
    private Date date;
    private String mainTopics;
    private String longDescription;

    public LaboratoryDTO() {

    }

    public LaboratoryDTO(String title, Date date, String mainTopics, String longDescription) {
        this.title = title;
        this.date = date;
        this.mainTopics = mainTopics;
        this.longDescription = longDescription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMainTopics() {
        return mainTopics;
    }

    public void setMainTopics(String mainTopics) {
        this.mainTopics = mainTopics;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
