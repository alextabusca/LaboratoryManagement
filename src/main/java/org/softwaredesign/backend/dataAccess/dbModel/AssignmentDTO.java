package org.softwaredesign.backend.dataAccess.dbModel;

import java.io.Serializable;
import java.sql.Date;

public class AssignmentDTO implements Serializable {

    private String title;
    private Date deadline;
    private String description;
    private LaboratoryDTO laboratoryDTO;

    public AssignmentDTO() {
    }

    public AssignmentDTO(String title, Date deadline, String description) {
        this.title = title;
        this.deadline = deadline;
        this.description = description;
        this.laboratoryDTO = laboratoryDTO;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LaboratoryDTO getLaboratoryDTO() {
        return laboratoryDTO;
    }

    public void setLaboratoryDTO(LaboratoryDTO laboratoryDTO) {
        this.laboratoryDTO = laboratoryDTO;
    }
}
