package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.SubmissionDTO;
import org.softwaredesign.backend.model.SubmissionModel;
import org.softwaredesign.backend.service.AssignmentService;
import org.softwaredesign.backend.service.StudentService;
import org.softwaredesign.backend.service.SubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/submissions")
public class SubmissionController {

    private final SubmissionService submissionService;
    private final StudentService studentService;
    private final AssignmentService assignmentService;

    @Autowired
    public SubmissionController(SubmissionService submissionService, StudentService studentService, AssignmentService assignmentService) {
        this.submissionService = submissionService;
        this.assignmentService = assignmentService;
        this.studentService = studentService;
    }

    @GetMapping("/getAllSubmissions")
    public List<SubmissionModel> getAllSubmissions() {
        List<SubmissionModel> submissions = submissionService.getAllSubmissions();

        for (SubmissionModel submission : submissions)
            System.out.println(submission);

        return submissionService.getAllSubmissions();
    }

    @GetMapping("/getSubmissionByID")
    public SubmissionModel getSubmissionByID(@RequestParam Long submissionID) {
        return submissionService.getSubmissionByID(submissionID);
    }

    @PostMapping("/saveSubmission")
    public SubmissionModel saveSubmission(@RequestBody SubmissionDTO submissionDTO) {
        return submissionService.saveSubmission(submissionDTO);
    }

    @PutMapping("/updateSubmission")
    public SubmissionModel updateSubmission(@RequestParam Long submissionID, @RequestBody SubmissionDTO submissionDTO) {
        return submissionService.updateSubmission(submissionID, submissionDTO);
    }

    @DeleteMapping("/deleteSubmissionByID")
    public String deleteSubmissionByID(@RequestParam Long submissionID) {
        submissionService.deleteSubmissionByID(submissionID);

        return "Submission with id = " + submissionID + " has been successfully deleted!";
    }
}
