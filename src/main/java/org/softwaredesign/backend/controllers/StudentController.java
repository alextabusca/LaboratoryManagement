package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.StudentDTO;
import org.softwaredesign.backend.service.StudentService;
import org.softwaredesign.backend.model.StudentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/getAllStudents")
    public List<StudentModel> getAllStudents() {
        List<StudentModel> students = studentService.getAllStudents();

        for (StudentModel student : students)
            System.out.println(student);

        return studentService.getAllStudents();
    }

    @GetMapping("/getStudentByID")
    public StudentModel getStudentByID(@RequestParam Long userID) {
        return studentService.getStudentByID(userID);
    }

    @PostMapping("/saveStudent")
    public StudentModel saveStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.saveStudent(studentDTO);
    }

    @GetMapping("/registerStudent")
    public StudentModel registerStudent(@RequestParam String token, @RequestParam String email, @RequestParam String password) {
        return studentService.register(token, email, password);
    }

    @GetMapping("/loginStudent")
    public StudentModel loginStudent(@RequestParam String email, @RequestParam String password) {
        return studentService.login(email, password);
    }

    @PutMapping("/updateStudent")
    public StudentModel updateStudent(@RequestParam Long studentID, @RequestBody StudentDTO studentDTO) {
        return studentService.updateStudent(studentID, studentDTO);
    }

    @DeleteMapping("/deleteStudentByID")
    public String deleteStudentByID(Long studentID) {
        studentService.deleteStudentByID(studentID);

        return "Student with id = " + studentID + " has been successfully deleted!";
    }
}
