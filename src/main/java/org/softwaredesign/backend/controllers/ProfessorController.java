package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.ProfessorDTO;
import org.softwaredesign.backend.model.ProfessorModel;
import org.softwaredesign.backend.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    @Autowired
    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping("/professorLogin")
    public List<ProfessorModel> professorLogin(@RequestParam String email, @RequestParam String password) {
        return professorService.professorLogin(email, password);
    }

    @GetMapping("/getAllProfessors")
    public List<ProfessorModel> getAllProfessors() {
        List<ProfessorModel> professors = professorService.getAllProfessors();

        for (ProfessorModel professor : professors)
            System.out.println(professor);

        return professorService.getAllProfessors();
    }

    @GetMapping("/getProfessorByID")
    public ProfessorModel getProfessorByID(@RequestParam Long professorID) {
        return professorService.getProfessorByID(professorID);
    }

    @PostMapping("/saveProfessor")
    public ProfessorModel saveProfessor(@RequestBody ProfessorDTO professorDTO) {
        return professorService.saveProfessor(professorDTO);
    }

    @PutMapping("/updateProfessor")
    public ProfessorModel updateProfessor(@RequestParam Long professorID, @RequestBody ProfessorDTO professorDTO) {
        return professorService.updateProfessor(professorID, professorDTO);
    }

    @DeleteMapping("/deleteProfessorByID")
    public String deleteProfessorByID(Long professorID) {
        professorService.deleteProfessorByID(professorID);
        return "Student with id = " + professorID + " has been successfully deleted!";
    }

}
