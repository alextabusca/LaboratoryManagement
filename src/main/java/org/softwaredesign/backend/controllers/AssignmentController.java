package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.AssignmentDTO;
import org.softwaredesign.backend.model.AssignmentModel;
import org.softwaredesign.backend.model.LaboratoryModel;
import org.softwaredesign.backend.service.AssignmentService;
import org.softwaredesign.backend.service.LaboratoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;
    private final LaboratoryService laboratoryService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService, LaboratoryService laboratoryService) {
        this.assignmentService = assignmentService;
        this.laboratoryService = laboratoryService;
    }

    @GetMapping("/getAllAssignments")
    public List<AssignmentModel> getAllAssignments() {
        List<AssignmentModel> assignments = assignmentService.getAllAssignments();

        for (AssignmentModel assignment : assignments)
            System.out.println(assignment);

        return assignmentService.getAllAssignments();
    }

    @GetMapping("/getAssignmentByID")
    public AssignmentModel getAssignmentByID(@RequestParam Long assignmentID) {
        return assignmentService.getAssignmentByID(assignmentID);
    }

    @GetMapping("/getAssignmentsByLaboratoryID")
    public List<AssignmentModel> getAssignmentsByLaboratoryID(@RequestParam Long laboratoryID) {
        LaboratoryModel laboratoryModel = laboratoryService.getLaboratoryByID(laboratoryID);

        if (laboratoryModel == null)
            return null;

        return assignmentService.getAssignmentsByLaboratoryID(laboratoryID);
    }

    @PostMapping("/saveAssignment")
    public AssignmentModel saveAssignment(@RequestBody AssignmentDTO assignmentDTO) {
        return assignmentService.saveAssignment(assignmentDTO);
    }

    @PutMapping("/updateAssignment")
    public AssignmentModel updateAssignment(@RequestParam Long assignmentID, @RequestBody AssignmentDTO assignmentDTO) {
        return assignmentService.updateAssignment(assignmentID, assignmentDTO);
    }

    @DeleteMapping("/deleteAssignmentByID")
    public String deleteAssignmentByID(Long assignmentID) {
        assignmentService.deleteAssignmentByID(assignmentID);
        return "Assignment with id = " + assignmentID + "has been successfully deleted";
    }
}
