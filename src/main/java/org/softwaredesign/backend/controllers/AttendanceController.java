package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.AttendanceDTO;
import org.softwaredesign.backend.model.AttendanceModel;
import org.softwaredesign.backend.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/attendances")
public class AttendanceController {

    private final AttendanceService attendanceService;

    @Autowired
    public AttendanceController(AttendanceService attendanceService) {
        this.attendanceService = attendanceService;
    }

    @GetMapping("/getAllAttendances")
    public List<AttendanceModel> getAllAttendances() {
        List<AttendanceModel> attendances = attendanceService.getAllAttendances();

        for (AttendanceModel attendance : attendances)
            System.out.println(attendance);

        return attendanceService.getAllAttendances();
    }

    @GetMapping("/getAttendanceByID")
    public AttendanceModel getAttendanceByID(@RequestParam Long attendanceID) {
        return attendanceService.getAttendanceByID(attendanceID);
    }

    @PostMapping("/saveAttendance")
    public AttendanceModel saveAttendance(@RequestBody AttendanceDTO attendanceDTO) {
        return attendanceService.saveAttendance(attendanceDTO);
    }

    @PutMapping("/updateAttendance")
    public AttendanceModel updateAttendance(@RequestParam Long attendanceID, @RequestBody AttendanceDTO attendanceDTO) {
        return attendanceService.updateAttendance(attendanceID, attendanceDTO);
    }

    @DeleteMapping("/deleteAttendanceByID")
    public String deleteAttendanceByID(Long attendanceID) {
        attendanceService.deleteAttendanceByID(attendanceID);

        return "Student with id = " + attendanceID + " has been successfully deleted!";
    }

    @GetMapping("/getAttendanceByLaboratoryID")
    public List<AttendanceModel> getAttendanceByLaboratoryID(@RequestParam Long laboratoryID) {
        return attendanceService.getAttendanceByLaboratoryID(laboratoryID);
    }
}
