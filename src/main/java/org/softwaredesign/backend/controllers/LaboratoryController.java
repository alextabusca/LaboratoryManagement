package org.softwaredesign.backend.controllers;

import org.softwaredesign.backend.dataAccess.dbModel.LaboratoryDTO;
import org.softwaredesign.backend.model.LaboratoryModel;
import org.softwaredesign.backend.service.LaboratoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/laboratories")
public class LaboratoryController {

    private final LaboratoryService laboratoryService;

    @Autowired
    public LaboratoryController(LaboratoryService laboratoryService) {
        this.laboratoryService = laboratoryService;
    }

    @GetMapping("/getAllLaboratories")
    public List<LaboratoryModel> getAllLaboratories() {
        List<LaboratoryModel> laboratories = laboratoryService.getAllLaboratories();

        for (LaboratoryModel laboratory : laboratories)
            System.out.println(laboratory);

        return laboratoryService.getAllLaboratories();
    }

    @GetMapping("/getLaboratoryByID")
    public LaboratoryModel getLaboratoryByID(@RequestParam Long laboratoryID) {
        return laboratoryService.getLaboratoryByID(laboratoryID);
    }

    @GetMapping("/searchAfterKeyword")
    public List<LaboratoryModel> searchAfterKeyword(@RequestParam String keyword) {
        return laboratoryService.searchAfterKeyword(keyword);
    }

    @PostMapping("/saveLaboratory")
    public LaboratoryModel saveLaboratory(@RequestBody LaboratoryDTO laboratoryDTO) {
        return laboratoryService.saveLaboratory(laboratoryDTO);
    }

    @PutMapping("/updateLaboratory")
    public LaboratoryModel updateLaboratory(@RequestParam Long laboratoryID, @RequestBody LaboratoryDTO laboratoryDTO) {
        return laboratoryService.updateLaboratory(laboratoryID, laboratoryDTO);
    }

    @DeleteMapping("/deleteLaboratoryByID")
    public String deleteLaboratoryByID(Long laboratoryID) {
        laboratoryService.deleteLaboratoryByID(laboratoryID);
        return "Laboratory with id = " + laboratoryID + " has been successfully deleted!";
    }

}
